
Chart.legend = function( entries ) {
	// NOTE: positioning handled by CSS.

	// Add a legend:
	var legend = {}, 
		height,
		symbolRadius = 5;

	legend.container = d3.select('body').append('div')
		.attr('class', 'legend');

	height = parseInt( d3.select('.legend').style('height'), 10);
	legend.canvas = legend.container.append('svg:svg')
			.attr('class', 'legend-canvas')
		.attr('transform', 'translate(0,-200)');

	legend.entries = legend.canvas.selectAll('.legend-entry')
		.data( entries )
	  .enter().append('svg:g')
	  	.attr('class', 'legend-entry')
	  	.attr('transform', function(d,i) { return 'translate('+ (symbolRadius + i*120) +', ' + (height/2) + ')'; });

	// Append circles to each entry with appropriate class:
	legend.entries.append('svg:circle')
		.attr('class', function(d) { return 'legend-symbol ' + d;} )
		.attr('r', symbolRadius )
		.attr('cy', 0 )
		.attr('cx', 0 );

	// Append text to each entry:
	legend.entries.append('svg:text')
		.attr('class', 'legend-text' )
		.attr('text-anchor', 'start')
		.attr('dy', '.35em')
		.attr('transform', 'translate(' + (symbolRadius*2) + ',0)')
		.text( function(d) { return d; } );

	// Add interactivity:
	legend.entries.on('mouseover.focus', mouseover)
		.on('mouseout.focus', mouseout);

	//
	function mouseover() {

		// Select the current element and get the symbol child class:
		var _class = d3.select( this ).select('.legend-symbol')
			.attr('class')
			.replace('legend-symbol ', ''); // left with legend class.

		d3.selectAll('.wedge')
			.filter( function(d,i) {
				// Select those elements not belonging to the same symbol class:
				return !d3.select( this ).classed( _class );
			})
			.transition()
				.duration( 1000 )
				.attr('opacity', 0.05 );

	}; // end FUNCTION mouseover()

	function mouseout() {

		d3.selectAll('.wedge')
			.transition()
				.duration( 500 )
				.attr('opacity', 1 );

	}; // end FUNCTION mouseout()

}; // end FUNCTION legend()

var rotateVal = 0, scaleVal = 1; trans_x=1, trans_y=1

function transformFig() {
	d3.selectAll('.wedgeGroup')
		.transition()
		.duration(500)
		.attr('transform', `rotate(${rotateVal}) scale(${scaleVal},${scaleVal}), translate(${trans_x},${trans_y})`);
}

Chart.slider = function( minVal, maxVal, step ) {

	d3.select('body').append('p').attr("class","slider").style("text-align","center").text("Wedge Display");
	d3.select('body').append('input')
		.attr('class', 'slider')
		.attr('type', 'range')
		.attr('name', 'slider')
		.attr('min', minVal)
		.attr('max', maxVal)
		.attr('step', 0.001)
		.attr('value', maxVal);

	d3.select("input").on("change", function() {
	  var value = Math.round(this.value);

	  d3.selectAll('.wedgeGroup')
	  	.filter( function(d,i) { return i < value; } )
	  	.transition()
	  		.duration( 1000 )
	  		// .attr( 'transform', 'scale(1,1)');
	  		.attr( 'opacity', '1');

	  d3.selectAll('.wedgeGroup')
	  	.filter( function(d,i) { return i >= value; } )
	  	.transition()
	  		.duration( 1000 )
	  		.attr( 'opacity', '0' );

	});


}; // end FUNCTION slider()


Chart.zoom = function( minVal, maxVal, step ) {
	d3.select('body').append('p').attr("class","slider").style("text-align","center").text("Zoom");
	d3.select('body').append('input')
		.attr('class', 'slider')
		.attr('id','zoom')
		.attr('type', 'range')
		.attr('name', 'slider')
		.attr('min', minVal)
		.attr('max', maxVal)
		.attr('step', step)
		.attr('value', (minVal + maxVal) / 2);

	d3.select("#zoom").on("change", function() {
		scaleVal = this.value;
		transformFig.call(this);
	});


}; // end FUNCTION slider()



Chart.rotateFig = function( minVal, maxVal, step, rose ) {

	d3.select('body').append('p').attr("class","slider").style("text-align","center").text("Rotate");
	d3.select('body').append('input')
		.attr('class', 'slider')
		.attr('id','rotate')
		.attr('type', 'range')
		.attr('name', 'slider')
		.attr('min', minVal)
		.attr('max', maxVal)
		.attr('step', step)
		.attr('value', (minVal + maxVal) / 2);

	d3.select("#rotate").on("change", function() {
		rotateVal = this.value;
		transformFig.call(this);
	});


};


Chart.translate = function( minVal, maxVal, step, rose ) {
	d3.select('body').append('p').attr("class","slider").style("text-align","center").text("Translate");
	d3.select('body').append('input')
		.attr('class', 'slider')
		.attr('id','translate')
		.attr('type', 'range')
		.attr('name', 'slider')
		.attr('min', minVal)
		.attr('max', maxVal)
		.attr('step', step)
		.attr('value', (minVal + maxVal) / 2);

	d3.select("#translate").on("change", function() {
		trans_x = this.value;
		transformFig.call(this);
	});


};
