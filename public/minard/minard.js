var  width=1600,height=900;
reload = function() {
    d3.selectAll("svg").remove()

    d3.select("body").append("svg").attr('width',width).attr('height',height).attr('id','minard').style("margin-left","210px")

    proj = d3.geo.mercator().translate([width/2,height*(1/5)]).center([31,55]).scale(6000)

    layout = d3.layout.trail()
        .positioner(function(d) {return proj([d.lon,d.lat])})

    data = layout
        .grouping(function(d) {return d.group})
        .data(napoleon.army)
        .coordType("xy")
        .layout()


//napoleon.army.map(function(d) {console.log( d.size,d.group)})

    widthScale =
        d3.scale.linear().domain([0,340000]).range([1,37])

    var lines = d3.select("#minard").selectAll("line").data(data)

    color = d3.scale.category10().domain([1,2,3])


    var time = 1500; //Time to get to Moscow
    longs = d3.extent(napoleon.army.map(function(d) {return d.x2}))
    negTime = d3.scale.linear().domain(longs).range([0,time])
    posTime = d3.scale.linear().domain(longs).range([time,0])

//Define a scale that returns right to left or left to right in time depending on the directionality. Well, not a scale, strictly, since it can't be inverted and so on: but a function that sits on top of scales.
    timeScale = function(d,element) {
        out = d.dir==-1? time : 0;
        lag = d.dir==-1 ? posTime(d[element]) : negTime(d[element]);
        return out+lag
    }

    napoleon.cities = napoleon.cities.map(function(d) {
        var point = proj([d.lon,d.lat])
        console.log(point)
        d.x = point[0]
        d.y = point[1]
        return d
    })

    var cities = d3
        .select("#minard")
        .selectAll("g")
        .data(napoleon.cities)

    color = ["#78836d","#F48427","#f4cf98","#C6F413"];
    lines.enter().append("line")
        .attr("x1",function(d) {return d.x1})
        .attr("y1",function(d) {return d.y1})
        .attr("y2",function(d) {return d.y1})
        .attr("x2",function(d) {return d.x1})
        .style("stroke-width",function(d) {return widthScale(d.size*2)})
        .style("stroke",function(d) {
            if(d.dir == -1)
                return color[0];
            else if(d.dir == 1 && d.group == 1)
                return color[2];
            else if(d.dir ==1 && d.group== 2)
                return color[1];
            else
                return color[3]}
            )
        .style("opacity",1)
        .transition()
        .ease("linear")//keeps the transition pointed in the right direction
        .duration(function(d) {return Math.abs(timeScale(d,'x2') - timeScale(d,'x1'))})
        .delay(function(d) { return d3.min([timeScale(d,'x1'),timeScale(d,'x2')]) })
        .attr("y2",function(d) {return d.y2})
        .attr("x2",function(d) {return d.x2})
        .style("stroke-linecap","round")

    // armysize
    lines.enter().append("text")
        .filter(d => d.hasOwnProperty("previous")==true)
        .text(d => {console.log(d);return d.size})
        .style("fill","black")
        .attr("transform",d => {
            let x_new = (d.x1 + d.next.x1)/2;
            let y_new = (d.y1 + d.next.y1)/2;
            return "translate(" + x_new + "," + y_new + ")"
        })
        // .delay(function(d) {return timeScale(d,'x')-150})
        // .duration(1000)
        .style("opacity",0.2)

    cities = cities.enter().append("g")
        .attr("transform",function(d) { return "translate(" + d.x + "," + d.y + ")" })




    circles = cities.append("circle").attr('r',3).style("fill","black").style("opacity",0).transition().delay(function(d) {return timeScale(d,'x')}).duration(1000).style("opacity",1)

// start drawing 150 milliseconds before the map gets there: this seems
// to produce the nicest effect.

    labels = cities.append("text").text(function(d) {return d.name})
        .style("fill","black").attr("transform","translate(5,-5)").style("opacity",0).transition()
        .delay(function(d) {return timeScale(d,'x')-150}).duration(1000).style("opacity",1)



    temps = d3.select("#minard").append("g").attr("transform","translate(0,400)")

    tempScale = d3.scale.linear().domain([1,-41]).range([0,200])

    yaxis = d3.svg.axis().orient("right").scale(tempScale)


    grid = temps.append("g")
        .attr("transform","translate(" + proj([37.6,55])[0] + "0)")

    temps.append("g")
        .attr("transform","translate(" + proj([37.6,55])[0] + "0)").attr("class","y axis").style("opacity",0)
        .call(yaxis)
        .transition()
        .delay(time)
        .style("opacity",1)

    temps
        .append("text")
        .style("font-size",50)
        .style("opacity",0)
        .attr("transform","translate(665,30)")
        .transition()
        .delay(time)
        .duration(1500)
        .text("Temperature in C")
        .style("opacity",.10)

    tmpLayout = d3.layout.trail()
        .positioner(function(d) {return [proj([d.lon,55])[0],tempScale(d.temp)]})

    napoleon.temp = napoleon.temp.map(function(d) {
        d.dir = -1; d.temp = d.temp*1.25;
        return d})
    tmpData = tmpLayout
        .grouping(function() {return 1})
        .data(napoleon.temp)
        .coordType("xy")
        .layout()
    console.log('tmpdata',tmpData);
    tlines = temps.selectAll("line.temperature").data(tmpData)

    var color_shade = d3.scale.linear()
        .domain([-36, -18, 0])
        .range([ "#110299","#0972ff","#0972FF"]);

    tlines
        .enter()
        .append("line")
        .attr("class","temperature")
        .attr("x1",function(d) {return d.x1})
        .attr("y1",function(d) {return d.y1})
        .attr("y2",function(d) {return d.y1})
        .attr("x2",function(d) {return d.x1})
        .style("stroke-width",6)
        .style("stroke", function (d) {return color_shade(d.temp)})
        .style("opacity",0.8)
        .transition()
        .ease("linear")
        .duration(function(d) {return Math.abs(timeScale(d,'x2') - timeScale(d,'x1'))})
        .delay(function(d) { return d3.min([timeScale(d,'x1'),timeScale(d,'x2')]) })
        .attr("y2",function(d) {return d.y2})
        .attr("x2",function(d) {return d.x2})
        .style("stroke-linecap","round")

    //temp bullets
    tlines.enter().append("circle").filter(d => d.hasOwnProperty("previous")==true)
        .attr('r',5)
        .attr("cx", d => d.x1)
        .attr("cy", d => d.y1)
        .style("fill","lightgrey")
        .style("opacity",1)
    //vertical lines
    tlines.enter().append("line")
        .attr("x1",function(d) {return d.x1})
        .attr("y1",function(d) {return -400})
        .attr("y2",function(d) {return +200})
        .attr("x2",function(d) {return d.x1})
        .style("stroke-width",1)
        .style("stroke","black")
        .style("opacity",0.3)


    grid
        .attr("class", "grid")
        .call(yaxis.orient("right")
            .tickSize(proj([25.3,55])[0] - proj([38,55])[0], 0, 0)
            .tickFormat("")
        )
        .style("opacity",0)
        .transition()
        .delay(time*2)
        //.duration(Math.abs(negTime(proj([25.3,55])[0])-negTime(proj([38,55])[0])))
        .duration(5000)
        .ease("linear")
        .call(yaxis.orient("right")
            .tickSize(proj([25.3,55])[0] - proj([38,55])[0], 0, 0)
            .tickFormat("")
        )
        .style("opacity",1)

    // temperature label
    tlines.enter().append("text").filter(d => d.hasOwnProperty("previous")==true)
        .text(d => {return d.previous.temp})
        .style("fill","black")
        .attr("transform",d => "translate(" + (d.x1 + 10) + "," + (d.y1 + 15)    + ")")
        .style("opacity",0.8)

    //date label
    tlines.enter().append("text").filter(d => d.hasOwnProperty("previous")==true)
        .text(d => d.previous.date)
        .style("fill","black")
        .attr("transform",d => "translate(" + d.x1 + "," + 215 + ")")
        .style("opacity",1)
        .style("font-size",10)

    //append text
    d3.select("#minard").append("g").attr("transform", "translate(100,683.33333333333331)")
        .append("text")
        .text("Figurative chart of the successive losses in men by the French army in the Russian campaign 1812-1813. Drawn up by Mr Minard,")
        .attr("class", "font-classic")
        .style("font-size","30px")
        .style("font-family","Niconne")
        .style("fill","black")
        .style("opacity","1")

    d3.select("#minard").append("g").attr("transform", "translate(100,713.33333333333331)")
        .append("text")
        .text("inspector-general of bridges and roads (retired). Paris, 20 November 1869. The number of men present is symbolised by the broadness")
        .attr("class", "font-classic")
        .style("font-size","30px")
        .style("font-family","Niconne")
        .style("fill","black")
        .style("opacity","1")

    d3.select("#minard").append("g").attr("transform", "translate(100,743.33333333333331)")
        .append("text")
        .text("of the coloured zones at a rate of one millimetre for ten thousand men; furthermore, those numbers are written across the zones. The gold color")
        .attr("class", "font-classic")
        .style("font-size","30px")
        .style("font-family","Niconne")
        .style("fill","black")
        .style("opacity","1")

    d3.select("#minard").append("g").attr("transform", "translate(100,773.33333333333331)")
        .append("text")
        .text("signifies the men who entered Russia, the dark green who got out of it. The data used to draw up this chart were found in the works of Messrs.")
        .attr("class", "font-classic")
        .style("font-size","30px")
        .style("font-family","Niconne")
        .style("fill","black")
        .style("opacity","1")

    d3.select("#minard").append("g").attr("transform", "translate(100,803.33333333333331)")
        .append("text")
        .text("Thiers, de Ségur, de Fezensac, de Chambray and the unpublished journal of Jacob, pharmacist of the French army since 28 October.")
        .attr("class", "font-classic")
        .style("font-size","30px")
        .style("font-family","Niconne")
        .style("fill","black")
        .style("opacity","1")

    d3.select("#minard").append("g").attr("transform", "translate(100,833.33333333333331)")
        .append("text")
        .text("The light green and orange part represents troups which got divided during march")
        .attr("class", "font-classic")
        .style("font-size","30px")
        .style("font-family","Niconne")
        .style("fill","black")
        .style("opacity","1")



    //Legend
    let x=1353.5987755982987
    let y = 290.761904761904762
    d3.select("#minard").append("g").attr("transform", "translate(" + x +  "," + y + ")")
        .append("circle")
        .attr("r",10)
        .style("font-size","20px")
        .style("fill", color[2])
        .style("opacity",1)

    d3.select("#minard").append("g").attr("transform", "translate(" + (x+15) +  "," + (y+5) + ")")
        .append("text")
        .text("Group1")
        .style("font-size","20px")
        .style("fill", "black")
        .style("opacity",1)

    d3.select("#minard").append("g").attr("transform", "translate(" + x +  "," + (y + 30) + ")")
        .append("circle")
        .attr("r",10)
        .style("font-size","20px")
        .style("fill", color[1])
        .style("opacity",1)

    d3.select("#minard").append("g").attr("transform", "translate(" + (x+15) +  "," + (y+35) + ")")
        .append("text")
        .text("Group2")
        .style("font-size","20px")
        .style("fill", "black")
        .style("opacity",1)


    d3.select("#minard").append("g").attr("transform", "translate(" + x +  "," + (y+60) + ")")
        .append("circle")
        .attr("r",10)
        .style("font-size","20px")
        .style("fill", color[3])
        .style("opacity",1)

    d3.select("#minard").append("g").attr("transform", "translate(" + (x+15) +  "," + (y+65) + ")")
        .append("text")
        .text("Group3")
        .style("font-size","20px")
        .style("fill", "black")
        .style("opacity",1)

    d3.select("#minard").append("g").attr("transform", "translate(" + x +  "," + (y+90) + ")")
        .append("circle")
        .attr("r",10)
        .style("font-size","20px")
        .style("fill", color[0])
        .style("opacity",1)

    d3.select("#minard").append("g").attr("transform", "translate(" + (x+15) +  "," + (y+95) + ")")
        .append("text")
        .text("Return Journey")
        .style("font-size","20px")
        .style("fill", "black")
        .style("opacity",1)



}

reload()